---
title: Who am I
subtitle: Why you'd want to hang out with me
comments: false
---

My name is Hian-Kun Tenn, a hobbyist of machine learning and Python programming.

I have learnt a lot from the open source communities and hope I could contribute
something back.

Have a nice day. :-)

