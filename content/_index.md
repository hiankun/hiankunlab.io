## About this site

Computer vision, maching learning, and programming are fun to learn
and to play with.
Whenever I learn something worth sharing I will post them here.

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io). Thank you all for the beautiful works.
