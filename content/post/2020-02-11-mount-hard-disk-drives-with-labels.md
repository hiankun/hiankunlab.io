---
title: Mount Hard Disk Drives with Labels
date: 2020-02-11
draft: false
tags: ["tools"]
---

Several months ago, I added two portable hard disk drives to my Lubuntu PC for
data backup.
One has the storage capacity of 1 TB and another is 2 TB.
Lubuntu mounts them automatically with the name of `Transcend` and `Transcend1`,
respectively.

The auto-mounting was great but the meaningless naming was absolutely not.
I always got confused about which one was which when I was checking data in 
these two HDDs,
but I had put the issue off until now.

Recently I revisited [i3wm](https://i3wm.org/), a brilliant tiling window manager
for GNU/Linux and BSD.
I had tried it several years ago but didn't keep using it with forgotten reasons.
Anyway I picked it back and found it won't mount my two external HDDs automatically.

That's a real problem for me and this time I decided to solve it permanently.

So the situation was:
I have to rename these two HDDs with meaningful names and then enable the
auto-mounting using `fstab` so that everything will be alright after system booting.

The following steps were done with the default Lubuntu 18.04 window manager,
which always auto-mounts the HDDs.
After all the setting steps, I switched to i3wm and everything worked as
expected.

# Check the Information of the HDDs

Before changing the HDDs' labels (i.e., names),
we need to check their status and relevant information which might be necessary
in later steps.

Here are some commands I found really useful:
`df -h`, `sudo blkid`, and `findmnt`.

List the mounted HDDs' status by using `df -h`:
```
$ df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/sde1       932G   47G  886G   5% /media/<your-user-name>/Transcend
/dev/sdd1       1.9T  213G  1.7T  12% /media/<your-user-name>/Transcend1
```
From the list we can make sure that the 1 TB HDD is `/dev/sde1` and it has been 
mounted as `Transcend` and so on.


Then use `sudo blkid` to view the labels and the UUID of the two HDDs:
```
$ sudo blkid
/dev/sdd1: LABEL="Transcend" UUID="3AB2AB5AB2AB1981" TYPE="ntfs" PARTUUID="4b1007b2-01"
/dev/sde1: LABEL="Transcend" UUID="86AEB68EAEB67671" TYPE="ntfs" PARTUUID="707e033e-01"
```
No wonder my Lubuntu auto-labelling them as `Transcend` and `Transcend1`,
respectively.
They have the same default labels after all.

Every time I tried to write the mounting settings in `/etc/fstab`,
I had a hard time to know the `OPTIONS` of my mounting targets.
~~Fortunately, this time Lubuntu has auto-mounted my two HDDs and I can use 
`findmnt` to view the settings:~~
```
$ findmnt /media/<your-user-name>/Transcend
TARGET               SOURCE    FSTYPE  OPTIONS
/media/<your-user-name>/Transcend /dev/sde1 fuseblk rw,nosuid,nodev,relatime,user_id=0,group_id=0,default_permissions,allow_other,blksize=4096
$ findmnt /media/<your-user-name>/Transcend1
TARGET                SOURCE    FSTYPE  OPTIONS
/media/<your-user-name>/Transcend1 /dev/sdd1 fuseblk rw,nosuid,nodev,relatime,user_id=0,group_id=0,default_permissions,allow_other,blksize=4096
```
~~The `OPTIONS` listed above will be copied and pasted to `/etc/fstab` later.
(Note that the lables in the `TARGET` column hasn't been renamed.)~~
(This time I found a solution, which is listed below in the `/etc/fstab` code block.)


# Change the Labels of the HDDs

Some goolging/duckduckgoing results suggest that I can change the HDDs' labels
by using GParted or do it in Windows.
These solutions seem easy, but I want to do it in my Linux system via simple
commands.

AFAIK, there are some commands which can rename the HDD's label for us.
For ext2/ext3/ext4 filesystem we have `e2label` and `tune2fs`.
In my case, however, using them causes error:
```
$ sudo e2label /dev/sde1 "transcend_1T"
e2label: Bad magic number in super-block while trying to open /dev/sde1
/dev/sde1 contains a ntfs file system labelled 'Transcend'
```
Because my HDDs are in the format of ntfs,
which need another tool named `ntfslabel`.
The command I used is as follows:
```
sudo ntfslabel -f /dev/sde1 "transcend_1T"
sudo ntfslabel -f /dev/sdd1 "transcend_2T"
```
Note: I don't remember when and how I installed `ntfslabel`.
If you have no `ntfslabel`, I guess (based on the man page of `ntfslabel`)
it is avaiable via installing `ntfs-3g`.

After renaming the labels, reboot the system and then check them by using `df -h`.

# Write the settings into fstab

The final step is to add the following lines to `/etc/fstab`:
```
# Transcend portable hard drives
UUID=86AEB68EAEB67671   /media/<your-user-name>/transcend_1T/  ntfs  rw,auto,nofail,user,fmask=133,dmask=022,uid=1000,gid=1000  0   0
UUID=3AB2AB5AB2AB1981   /media/<your-user-name>/transcend_2T/  ntfs  rw,auto,nofail,user,fmask=133,dmask=022,uid=1000,gid=1000  0   0
```
The options are copied from [here](https://unix.stackexchange.com/a/511936).

Reboot the system and switch to i3wm,
I finally have the two external HDDs auto-mounted with customized labels. 🎉

---

* Edit [2020-02-27]:
  Add `nofail` option in `/etc/fstab` to let the booting prcedure skip the 
  unplugged disk, or the system cannot boot properly.
