---
title: Try Geometric Transformation in Javascript
date: 2020-01-31
draft: true
tags: ["computer vision"]
---


\\[
\begin{bmatrix}
\text{Scale}_X & \text{Shear}_X & \text{Shift}_X \\\\ \text{Scale}_Y & \text{Shear}_Y & \text{Shift}_Y
\end{bmatrix}
\cdot
\begin{bmatrix}
x \\\\ y \\\\ 1
\end{bmatrix}
= \begin{bmatrix}
x^\prime & y^\prime
\end{bmatrix}
\\]

Code: [py_sandbox/geo_transforms](https://gitlab.com/hiankun/py_sandbox/tree/master/geo_transforms)
