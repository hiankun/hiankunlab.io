---
title: Some Information for OpenVINO on ARM boards
date: 2021-03-04
draft: false
tags: ["openvino", "tools", "arm", "embedded"]
---

(This post is a simple note for further reference.)

I have tried to install OpenVINO on Raspberry Pi Zero W but failed.
After searching, I started to be aware of something about ARM architectures.

RPi0W use [armv6](https://raspberrypi.stackexchange.com/a/83379) architecture,
OpenVINO cannot be installed with it.

Here are some might-be-useful links for installing OpenVINO on ARM boards:
* [Build OpenVINO Inference Engine](https://github.com/openvinotoolkit/openvino/wiki/BuildingCode)
* [Arm7 Single Board Computers and the Intel NCS 2](https://software.intel.com/content/www/us/en/develop/articles/arm-sbc-and-ncs2.html) (login needed)
* [Arm 64 Single Board Computers and the Intel NCS 2](https://software.intel.com/content/www/us/en/develop/articles/arm64-sbc-and-ncs2.html) (login needed)
* OpenVINO [source](https://github.com/openvinotoolkit/openvino)
* [List of ARM microarchitectures](https://en.wikipedia.org/wiki/List_of_ARM_microarchitectures)
