---
title: The Workflow of Training Models using TensorFlow Object Detection API
date: 2020-06-30
draft: true
tags: ["deep learning", "tensorflow"]
---

# Install the TF Object Detection API

My system is Lubuntu 18.04.4 and I chosed the mininum installation.
It has no Python 2 and it might cause some problems when setting up
`pycocotools`.

Follow [the Installation document](
https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md).

## (Optional) Create conda environment
```
git clone https://github.com/tensorflow/models.git
```
### Install TensorFlow (1.14) and dependencies
```
$ conda create -n tf_1_14_env python=3 -c conda-forge
$ conda activate tf_1_14_env
(tf_1_14_env) $ conda install -c conda-forge tensorflow-gpu==1.14
```

(Optional) Install `numpy<1.17` by doing
```
(tf_1_14_env) $ conda install "numpy<1.17" -c conda-forge
```
to prevent following warnings when importing `tensorflow`:
```
>>> import tensorflow as tf                                                                   
/home/thk/miniconda3/envs/tf_1_14_env/lib/python3.7/site-packages/tensorflow/python/framework/dtypes.py:516: FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated$ in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.
  _np_qint8 = np.dtype([("qint8", np.int8, 1)])
```

Install other libraries:
```
conda install -c conda-forge cython contextlib2 pillow lxml jupyter matplotlib pycocotools
```

### Install COCO API
This method failed in my system.

```
cd ..
git clone https://github.com/cocodataset/cocoapi.git
cd cocoapi/PythonAPI/
make
cp -r pycocotools ../../models/research/
```
Error:
```
ImportError: /media/thk/workspace/github_project/models/research/pycocotools/_mask.cpython-37m-x86_64-linux-gnu.so: failed to map segment from shared object
```

I also try [philferriere/cocoapi](https://github.com/philferriere/cocoapi) 
which supports Python 3,
but still get identical error.

I solved the problem by installing `pycocotools` via `conda`:
```
conda install -c conda-forge pycocotools
```
However, later when I ran the training script, which returned the following
error message:
```
tensorflow Invalid argument: TypeError: object of type <class 'numpy.float64'> cannot be safely interpreted as an integer.
```
After downgrading the `numpy` version by using
```
conda install "numpy<1.17" -c conda-forge
```
the error went away and finally I could start the train process.

### Protobuf
Remember to run this in conda env:
```
protoc object_detection/protos/*.proto --python_out=.
```

### Add path
Add the following line in `~/.bashrc`:
```
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
```
Remember to replace \`pwd\` with your path to `models/research`.
In my case it is
`/media/thk/workspace/github_project/models/research`.

In the same `models/research` folder, run the following command
to test the installation:
```
python object_detection/builders/model_builder_test.py
```
# Data Preparation
## Collect the images

## Preprocessing
### Resize large images
## Label the images

The labelling tools:

* VIA
* LabelImg
* Labelme

## (Optional) Augment the labelled images

# Prepare the TFRecord files

* Concate all the annotations into single csv file with the following row
  at the beginning:
  ```
  filename,width,height,class,xmin,ymin,xmax,ymax
  ```


# Download the Pre-Trained Model

Error: [https://github.com/tensorflow/models/issues/4066](https://github.com/tensorflow/models/issues/4066)
For `ssd_mobilenet_v2_coco`,
comment out line 35 in the config file:
```
35       #batch_norm_trainable: true
```
# Fine-Tune

# Test the Fine-Tuned Model

# Deploy to edge devices

[The document](https://github.com/tensorflow/models/tree/master/research/object_detection)

---
SSD:

https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md

https://towardsdatascience.com/how-to-train-your-own-object-detector-with-tensorflows-object-detector-api-bec72ecfe1d9

---

# Mask R-CNN

Prepare the training data for Mask R-CNN had confused me for a long time.
At the beginning, I can only train one-class models by following the example of 
[Splash of Color](https://engineering.matterport.com/splash-of-color-instance-segmentation-with-mask-r-cnn-and-tensorflow-7c761e238b46)
and couldn't understand how to expand the sample code for multiple classes 
problems.

I knew that every instance has a corresponding mask indicating its position in
the image at pixel level.
However, I didn't understand how to set the "class ID" to each instance.

Check the source code of [Mask_RCNN/samples/shapes/shapes.py](
https://github.com/matterport/Mask_RCNN/blob/cbff80f3e3f653a9eeee43d0d383a0385aba546b/samples/shapes/shapes.py)

![](/pics/20200630_def_load_mask.png)

ref: my own reply on SO:
https://stackoverflow.com/questions/49629933/ground-truth-pixel-labels-in-pascal-voc-for-semantic-segmentation/61425574#61425574
