---
title: Sync A Forked Repo To The Upstream One
date: 2020-02-08
draft: true
tags: ["tools"]
---

[https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/syncing-a-fork](
Syncing a fork)

Fork a repo (our upstream repo) on GitHub, 
then clone the forked repo to our local machine.

## configure a remote that points to the upstream repo
https://help.github.com/en/articles/configuring-a-remote-for-a-fork

Use `git branch -a` to show all the branches:
```
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
```

Use `git remote -v` to list the configured remote repo (the forked one):
```
origin  https://github.com/hiankun/handson-ml2.git (fetch)
origin  https://github.com/hiankun/handson-ml2.git (push)
```

Add the upstream repo:
```
git remote add upstream https://github.com/ageron/handson-ml2.git
```

Check the setting by using `git remote -v` again. 
Now we have the added upstream:
```
origin  https://github.com/hiankun/handson-ml2.git (fetch)
origin  https://github.com/hiankun/handson-ml2.git (push)
upstream  https://github.com/ageron/handson-ml2.git (fetch)
upstream  https://github.com/ageron/handson-ml2.git (push)
```

TODO: The following part need upstream repo to update for further experiments...
## work in local forked repo
thk (master) handson-ml2 $ git fetch upstream 
From https://github.com/ageron/handson-ml2
 * [new branch]      master     -> upstream/master
thk (master) handson-ml2 $ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
  remotes/upstream/master

