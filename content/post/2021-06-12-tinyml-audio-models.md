---
title: TinyML Audio Models are not/too simple
date: 2021-06-12
draft: true
tags: ["embedded", "deep learning", "tensorflow"]
---

The `tiny_conv` model is too small to be useful. [1]

In pp.164 of TinyML book:
> The model we're using is small and imprefect, and you'll probably
> notice that it's better at detecting "yes" than "no."...

# References
[1] [Pete Warden talk about "tiny_conv"](https://github.com/adafruit/Adafruit_TFLite_Micro_Speech/issues/2)
[2] Follow this: [Simple audio recognition: Recognizing keywords](https://www.tensorflow.org/tutorials/audio/simple_audio)
[3] [tensorflow/tflite-micro](https://github.com/tensorflow/tflite-micro)

