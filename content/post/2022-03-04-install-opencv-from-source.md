---
title: Install OpenCV 4.5.5 from Source in Xubuntu 20.04
date: 2022-03-04
draft: false
tags: ["opencv", "tools"]
---

For some reasons, I have to install OpenCV from source in my Xubuntu 20.04.
To me, the tricky part was the options for CMake.
Here I share my settings and the reason why I need them.

# Install required packages

Source: [How to Install OpenCV on Ubuntu 20.04](https://linuxize.com/post/how-to-install-opencv-on-ubuntu-20-04/)

```
sudo apt install build-essential cmake git pkg-config libgtk-3-dev \
    libavcodec-dev libavformat-dev libswscale-dev libv4l-dev \
    libxvidcore-dev libx264-dev libjpeg-dev libpng-dev libtiff-dev \
    gfortran openexr libatlas-base-dev python3-dev python3-numpy \
    libtbb2 libtbb-dev libdc1394-22-dev libopenexr-dev \
    libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev
```

# Download the source

There are two sources I downloaded for installation.
One is of course the [opencv](https://github.com/opencv/opencv)
and another is the [opencv_contrib](https://github.com/opencv/opencv_contrib).
The later one is essential for some modules such as the DNN one for
deep learning neural networks.
Both of them could be found under [OpenCV GitHub repo](https://github.com/opencv).

As the moment of this post, the latest version was 4.5.5.
Download them (
[opencv-4.5.5.tar.gz](https://github.com/opencv/opencv/releases/tag/4.5.5) and 
[opencv_contrib-4.5.5.tar.gz](https://github.com/opencv/opencv_contrib/releases/tag/4.5.5))
and extract them in the same folder.

# Installation

## Find GPU information

If you need to run OpenCV with your CUDA-enabled GPU,
follow the next two steps to get the necessary information.

1. Use `nvidia-smi -L` to list your GPU name.
1. Go to the page of [GPU Compute Capability](https://developer.nvidia.com/cuda-gpus) 
  to find the GPU compute capability,
  which is the value of `CUDA_ARCH_BIN` in the `cmake` options.

(What if I have multiple GPUs and they have different `CUDA_ARCH_BIN` values?
Sorry I have no such cool resources and don't know how to set the option...)

## Some CMake options

* The options of `CUDA` or `CUDNN` should be obvious.
  If you just want to use OpenCV with CPU, then set them to `OFF`.
* The `OPENCV_GENERATE_PKGCONFIG` is used to generate `opencv4.pc` file,
  which might be needed for some other packages.
  In my own case, I need it to install [darknet](https://github.com/AlexeyAB/darknet) for YOLO.
* `OPENCV_EXTRA_MODULES_PATH` points to the `opencv_contrib` folder.
* __NOTE__ 1: For OpenCV 4.5.5, we need to set `-D PYTHON3_PACKAGES_PATH` to `dist-packages` as listed in the following section, or the Python packages will be installed to `site-packages` and we cannot import cv2 properly. (ref: [Opencv issue #21359](https://github.com/opencv/opencv/issues/21359#issuecomment-1003005474))
* __NOTE__ 2: You may need to install `libcudnn8` and `libcudnn8-dev` if you want to build OpenCV with the `dnn` module (with `-D OPENCV_DNN_CUDA=ON` in `cmake`).

## Install OpenCV

The steps are as follows.

* `cd` to the downloaded and extracted `opencv-4.5.5` folder.
* `make build && cd build`
* Run the following command 

  ```
  cmake -D CMAKE_BUILD_TYPE=RELEASE \
      -D OPENCV_GENERATE_PKGCONFIG=ON \
      -D CMAKE_INSTALL_PREFIX=/usr/local \
      -D INSTALL_PYTHON_EXAMPLES=ON \
      -D INSTALL_C_EXAMPLES=OFF \
      -D OPENCV_ENABLE_NONFREE=ON \
      -D WITH_CUDA=ON \
      -D WITH_CUDNN=ON \
      -D OPENCV_DNN_CUDA=ON \
      -D ENABLE_FAST_MATH=1 \
      -D CUDA_FAST_MATH=1 \
      -D CUDA_ARCH_BIN=6.1 \
      -D WITH_CUBLAS=1 \
      -D OPENCV_EXTRA_MODULES_PATH=/home/user/opencv_contrib-4.5.1/modules/\
      -D HAVE_opencv_python3=ON \
      -D PYTHON3_PACKAGES_PATH=/usr/lib/python3/dist-packages \
      -D BUILD_EXAMPLES=ON ..  
  ```
* `make -j$(nproc)` (`nproc` returns the available number of your processors)
* `sudo make install`
* `sudo ldconfig`

# Check the installation

* We can check the installation by `pkg-config --modversion opencv4`.
  The terminal will show the version number (`4.5.5` in this case).
* In Python:

  ```
  $ python3
  Python 3.8.10 (default, Nov 26 2021, 20:14:08)
  [GCC 9.3.0] on linux
  Type "help", "copyright", "credits" or "license" for more information.
  >>> import cv2
  >>> cv2.__version__
  '4.5.5
  ```

* In C++, use the following code:

  ```cpp
  #include <opencv2/opencv.hpp>
  #include <iostream>
  
  int main() {
      std::cout << "OpenCV version: " << cv::getBuildInformation().c_str() << "\n";
  
      return 0;
  }
  ```
  and compile it using
  ```
  g++ cv_build_info.cpp -o cv_build_info `pkg-config --cflags --libs opencv4`
  ```
  Run `./cv_build_info` to view all the build information including the installed version.
