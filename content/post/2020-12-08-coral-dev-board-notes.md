---
title: Revisit the Coral Dev Board
date: 2020-12-08
draft: false
tags: ["embedded", "tensorflow", "coral"]
---


Quite a while ago, 
I tried to setup the [Coral Dev Board](https://coral.ai/products/dev-board/) but failed.
This time I follow the guide again and get the setup done,
but still encounter some problems.
Here is an imcomplete note of my test.
Hope I could find some time to make it useful to my projects...

# Reflash the board

* Downloaded image: `enterprise-eagle-20200724205123.zip`

# Cannot run the default demos

The document [Run a demo with the camera](https://coral.ai/docs/dev-board/camera/#run-a-demo-with-the-camera) read:

> The Mendel system image on the Dev Board includes two demos that perform 
> real-time image classification and object detection with the Edge TPU API.

However, there were no `edgetpu_classify` or `edgetpu_detect` in the system.

Also, another part of the document stated that the Edge TPU API is deprecated:

> This API is deprecated: Instead try the PyCoral API.

Maybe the new Medel OS has no the default demos?

# Try PyCoral API examples

Clone the repo of [google-coral/pycoral](https://github.com/google-coral/pycoral)
to run the examples.

## Classification
* Command:
  ```
  python3 classify_image.py \
  	-m ../test_data/mobilenet_v2_1.0_224_quant_edgetpu.tflite \
  	-i ../test_data/bird.bmp \
  	-l ../test_data/imagenet_labels.txt
  ```
* Output:
  ```
  ----INFERENCE TIME----
  Note: The first inference on Edge TPU is slow because it includes loading the model into Edge TPU memory.
  12.9ms
  2.6ms
  2.5ms
  2.5ms
  2.5ms
  -------RESULTS--------
  chickadee: 0.98047
  ```

## Detection
* Command:
  ```
  python3 detect_image.py \
  	-m ../test_data/ssd_mobilenet_v2_face_quant_postprocess_edgetpu.tflite \
  	-i ../test_data/face.jpg \
  	-l ../test_data/coco_labels.txt
  ```

* Output:
  ```
  ----INFERENCE TIME----
  Note: The first inference is slow because it includes loading the model into Edge TPU memory.
  50.65 ms
  29.95 ms
  28.92 ms
  29.95 ms
  23.17 ms
  -------RESULTS--------
  person
    id:     0
    score:  0.99609375
    bbox:   BBox(xmin=102, ymin=52, xmax=244, ymax=217)
  person
    id:     0
    score:  0.99609375
    bbox:   BBox(xmin=238, ymin=20, xmax=370, ymax=177)
  person
    id:     0
    score:  0.83203125
    bbox:   BBox(xmin=1, ymin=87, xmax=64, ymax=162)
  person
    id:     0
    score:  0.5
    bbox:   BBox(xmin=432, ymin=102, xmax=503, ymax=187)
  ```
