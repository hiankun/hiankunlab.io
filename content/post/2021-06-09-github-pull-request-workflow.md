---
title: Workflow of Pull Request using Git
date: 2021-06-09
draft: false
tags: ["git", "tools"]
---

Recently I was really lucky to push two pull requests [1] to the great
[Streamlit - Drawable Canvas](https://github.com/andfanilo/streamlit-drawable-canvas)
repo and both of them had been merged.
I said I was lucky because this is my first time to do PR to other developer's project.
The project creator [Fanilo Andrianasolo](https://github.com/andfanilo) is nice and instructive,
who helped me to overcome the barriers during the working process.
Because I am totally unfamiliar with Typescript and most of the frontend technologies,
without his kind help I won't make my contribution to the project.

Another barrier is that I have never really understand the git concept and commands
which are essential for anyone who want to contribute to other repos.
I tried to search and experiment some commands and also lucky enough to accumulate some basic understandings.

Here are some notes I would like to write down for my future self.
The following notes use the `streamlit-drawable-canvas` as the example.

We will have three repos: the upstream one, the forked one, and the local one.
The upstream repo is the original (not the `original` in `git`) one belongs to someone else.
The forked repo is what we forked from the upstream one.
Finally, the local repo refers to the one we cloned to our own local machine.

The following steps have not been completed yet:

* Fork the target repo of course.
* Clone our forked repo to your local machine by `git clone`.
* In our local repo, add the upstream repo as our remote by `git remote add upstream https://github.com/andfanilo/streamlit-drawable-canvas.git`:.
    * After doing this, we can view our remotes by `git remote -v`. Mine is as the follows:
        ```
        origin  https://github.com/hiankun/streamlit-drawable-canvas.git (fetch)
        origin  https://github.com/hiankun/streamlit-drawable-canvas.git (push)
        upstream        https://github.com/andfanilo/streamlit-drawable-canvas.git (fetch)
        upstream        https://github.com/andfanilo/streamlit-drawable-canvas.git (push)
        ```
* Suppose I want to do some development on the basis of the upstream repo, I should create a new branch from the branch which I will push a PR to.
    * For example, after complete my works I want to push the PR to the `develop` branch of the upstream repo, then
    * I have to checkout (switch to) the `develop` branch in my local repo: `git checkout develop`. 
    * In this branch, I create a new branch and switch to it for future works: `git checkout -b my_new_branch`.
    * Start to coding...
* When we need to commit the changes from the local machine to our forked repo, use `git push original my_new_branch`.
* If we need to copy some files from `branch_A` to `branch_B`, do the following:
    ```
    git checkout branch_B
    git checkout branch_A -- all the files you want from branch_A
    ```
    * AFAIK, the `--` lets git know that all the arguments afterward are files.
* After our lucky work has been merged, we can delete the branch in the local repo and the forked repo.
    * Use `git branch -d my_new_branch` to delete the branch in local repo. (If git complains, use `-D` to force it.)
    * Use `git push origin --delete polypath` to delete the branch in forked repo.

---

[1] [Why is a git 'pull request' not called a 'push request'?](https://stackoverflow.com/questions/21657430/why-is-a-git-pull-request-not-called-a-push-request)
