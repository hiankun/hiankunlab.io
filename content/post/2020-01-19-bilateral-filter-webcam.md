---
title: Using Bilateral Filter with Webcam
date: 2020-01-19
draft: false
tags: ["computer vision"]
---

![SIPI Pepper image](/pics/20200119_peppers_res.png)


```python
cv2.bilateralFilter(src, d, sigmaColor, sigmaSpace[, dst[, borderType]])
```

If you want to apply `cv2.bilaterFilter()` in near real-time applications 
such as using webcam or read video stream as the image input,
here're some tricks:

* The value of `sigmaSpace` should be kept as small as possible.

  When using `sigmaSpace` larger than 5, for example, with 640x480 input size,
  the processing time will make your application extremely laggy.

* Or you can scale down the original image to apply `bilateralFilter` on,
  so that the bilateral filter can process the smaller input faster.
  After that, just scale the filtered image back to its original size.
  Usually, the filtered differences caused by the scaling are not a problem.

Further information can be found in [OpenCV document](
https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html#bilateralfilter).

Here is a smple code to test the bilateral filter.

```python
import numpy as np
import cv2


sigmaColor = 80
sigmaSpace = 9 ## large value will slow down the program dramatically

frame = cv2.imread('./pics/peppers.png', 1)
smoothed = cv2.bilateralFilter(frame, -1, sigmaColor, sigmaSpace)
res = np.hstack((frame, smoothed))
cv2.imwrite('./pics/peppers_res.png', res)

cv2.destroyAllWindows()
```
